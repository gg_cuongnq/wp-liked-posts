<?php
/*
Plugin Name: WP Liked Posts
Plugin URI: http://wordpress.local
Description: Enables you to display button like post
Version: 1.0
Author: cuongnq@greenglobal.vn
Author URI: http://wordpress.local
Text Domain: WP Liked Posts
*/

/*
	Copyright 2016  Ngo Quang Cuong  (email : cuongnq@greenglobal.vn)
*/

// add field to setting in general
function register_facebook_connect() {
   register_setting('general', '_facebook_connect', 'esc_attr');
   add_settings_field('_facebook_connect', '<label for="_facebook_connect">'.__('Facebook connect' , '_facebook_connect' ).'</label>' , 'print_facebook_connect', 'general');
}
function print_facebook_connect() {
    $value = get_option( '_facebook_connect', '');
    if(empty($value)):
        $value = '<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.5&appId=1041558552562303";
  fjs.parentNode.insertBefore(js, fjs);
}(document, "script", "facebook-jssdk"));
</script>';
    endif;
    echo '<textarea name="_facebook_connect" id="_facebook_connect" class="large-text code" rows="10">' . $value . '</textarea>';
}

add_filter('admin_init', 'register_facebook_connect');

function display_button_like_post($liked=0) {
    $url_news = get_permalink(get_the_ID());
    if (!$post_views = get_post_meta((int)get_the_ID(), '_liked_posts', true )) {
        $post_views = $liked;
    }
    $value = get_option( '_facebook_connect', '');
    if(empty($value)):
        $value = '<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.5&appId=1041558552562303";
  fjs.parentNode.insertBefore(js, fjs);
}(document, "script", "facebook-jssdk"));
</script>';
    endif;
    $value = wp_specialchars_decode($value, 'ENT_QUOTES');
    echo '<div class="liked-posts"><button href="#" class="btn btn-default" id="liked-posts-button"><span class="dashicons dashicons-heart"></span>'.__('Thích').' (<span class="counter-liked">'.$post_views.'</span>)</button><input type="hidden" value="'.(int)get_the_ID().'" id="postID_value" name="postID_value"/>'
            . '<div class="share-button">
                    <div class="fb-like" data-href="'.$url_news.'" data-layout="box_count" data-action="like" data-show-faces="true" data-share="false"></div>
                </div>
               <div class="g-plusone-share">
                    <!-- Place this tag where you want the +1 button to render. -->
                    <div class="g-plusone" data-size="tall" data-annotation="bubble" data-width="50" data-href="'.$url_news.'" expandTo="right"></div>
                </div> 
</div>
'.$value;
}

add_action('wp_ajax_nopriv_add_liked_posts', 'add_liked_posts');
add_action('wp_ajax_add_liked_posts', 'add_liked_posts');

function display_rating_votes() {
    $post_liked = get_post_meta((int)get_the_ID(), '_liked_posts', true );
    if (!empty($post_liked)) {
    ?>
    <div class="average-rating">
        <?php 
            $percentRating = 100;
        ?>
        <div class="ratings">
            <div class="rating-box">
            <div class="rating" style="width:<?php echo $percentRating;?>%"></div>
            </div>
            <p class="rating-links">
                <span itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating">
                    <span itemprop="ratingValue">5</span> stars dựa trên
                    <span itemprop="ratingCount"><?php echo $post_liked;?></span> bình chọn
                </span>
            </p>
        </div>
    </div>
    <?php
    }
}

function add_liked_posts() {
    if ( 'POST' != $_SERVER['REQUEST_METHOD'] ) {
        header('Allow: POST');
        header('HTTP/1.1 405 Method Not Allowed');
        header('Content-Type: text/plain');
        exit;
    }
	if(isset($_POST['postId'])&&($_POST['postId']!='')) {
        if(get_post_status($_POST['postId']) === FALSE) {
            echo 'The post does not exist';
            exit();
        }
        if (!$post_views = get_post_meta((int)$_POST['postId'], '_liked_posts', true )) {
            $post_views = 0;
            add_post_meta((int)$_POST['postId'], '_liked_posts', '1');
            echo '1';
            exit;
        }else {
            $liked_posts = (int)get_post_meta((int)$_POST['postId'], '_liked_posts', true);
            update_post_meta((int)$_POST['postId'], '_liked_posts', ($liked_posts+1));
            echo get_post_meta((int)$_POST['postId'], '_liked_posts', true);
            exit;
        }
    }
}

//add style liked posts to theme
wp_enqueue_style('wp-liked-posts', plugins_url('/wp-liked-posts.css', __FILE__ ), array(), '1.0' );

add_action('wp_enqueue_scripts', 'liked_posts_ajax_enqueue_scripts','9998');
function liked_posts_ajax_enqueue_scripts() {
	wp_enqueue_script( 'liked_posts-ajax', plugins_url( '/wp-liked-posts.js', __FILE__ ), array(), '1.0', true );
}