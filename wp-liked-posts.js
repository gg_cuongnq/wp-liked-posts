$('#liked-posts-button').on('click',function(){
    $(this).attr('disabled',true);
    $('#liked-posts-button .dashicons-heart').after('<img class="liked-post-loading" src="/wp-content/plugins/wp-liked-posts/loading.gif"/>');
    var formData = new FormData();
    formData.append('postId', $('#postID_value').val());
   $.ajax({
        url: '/wp-admin/admin-ajax.php?action=add_liked_posts',
        data: formData,
        processData: false,
        contentType: false,
        type: 'POST',
        dataType: 'text',
        success: function (response) {
            $('.liked-posts .counter-liked').text($.trim(response,'0'));
            //$('#liked-posts-button').attr('disabled',false);
            $('#liked-posts-button .liked-post-loading').remove();
        }
    });
    return false;
});